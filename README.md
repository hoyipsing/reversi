# How to run

```
java -classpath target/reversi-game-0.0.1.jar learn.reversi.ReversiConsoleApp
```

# How to build

```
mvn clean package
```
# How to build report

```
mvn clean package site
```
