package learn.reversi;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class CoordinateCodecRowLabelTest {
	@Parameters(name = "{index}: rowIndexToLabel({0})={1}")
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] {
			{ -1, null },
			{ 0, "1" },
			{ 1, "2" },
			{ 2, "3" },
			{ 3, "4" },
			{ 4, "5" },
			{ 5, "6" },
			{ 6, "7" },
			{ 7, "8" },
		});
	}

    @Parameter
    public int rowIndex;
    @Parameter(1)
    public String expectedRowLabel;

	@Test
	public void encodeRowLabel() {
		assertThat("Invalid row label encoded",
				CoordinateCodec.rowIndexToLabel(this.rowIndex),
				equalTo(this.expectedRowLabel));
	}

}
