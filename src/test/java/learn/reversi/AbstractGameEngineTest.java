package learn.reversi;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Scanner;

import org.junit.After;
import org.junit.Before;

public abstract class AbstractGameEngineTest {

	protected ByteArrayOutputStream os;
	private StringWriter captureStringWriter;
	protected PrintWriter captureWriter;

	public AbstractGameEngineTest() {
		super();
	}

	@Before
	public void setup() {
		this.os = new ByteArrayOutputStream();
		this.captureStringWriter = new StringWriter();
		this.captureWriter = new PrintWriter(this.captureStringWriter, true);
	}

	@After
	public void printOutput() {
		System.out.println(new String(this.os.toByteArray()));
	}

	protected abstract String getCaseFolder();

	protected InputStream createInputStreamFromFile(String fileName) {
		try {
			return new FileInputStream(new File(getCaseFolder(), fileName));
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	protected int countNumberOfInputRound(String inputFile) throws FileNotFoundException {
		int count = 0;
		try (Scanner scanner = new Scanner(new File(getCaseFolder(), inputFile))) {
			while (scanner.hasNextLine()) {
				++count;
				scanner.nextLine();
			}
		}
		return count;
	}

	protected void assertBoardState(String expectedBoardStateFile) {
		try (final Scanner expectedScanner = new Scanner(new File(getCaseFolder(), expectedBoardStateFile));
			final Scanner actualScanner = new Scanner(this.captureStringWriter.toString());) {
			while (expectedScanner.hasNextLine()) {
				assertThat("Invalid board state", actualScanner.nextLine(), equalTo(expectedScanner.nextLine()));
			}
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

}