package learn.reversi;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.IntStream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class GameEngineValidInputTest extends AbstractGameEngineTest {
	private static final String CASE_DIR = "src/test/resources/game001";

	@Parameters(name = "{index}: input:{0} expected output:{1}")
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] {
			{ "001_INPUT.txt", "001_OUTPUT.txt" },
			{ "002_INPUT.txt", "002_OUTPUT.txt" },
			{ "003_INPUT.txt", "003_OUTPUT.txt" },
			{ "004_INPUT.txt", "004_OUTPUT.txt" },
			{ "005_INPUT.txt", "005_OUTPUT.txt" },
			{ "006_INPUT.txt", "006_OUTPUT.txt" },
			{ "007_INPUT.txt", "007_OUTPUT.txt" },
			{ "008_INPUT.txt", "008_OUTPUT.txt" },
			{ "009_INPUT.txt", "009_OUTPUT.txt" },
			{ "010_INPUT.txt", "010_OUTPUT.txt" },
			{ "011_INPUT.txt", "011_OUTPUT.txt" },
			{ "012_INPUT.txt", "012_OUTPUT.txt" },
			{ "013_INPUT.txt", "013_OUTPUT.txt" },
			{ "014_INPUT.txt", "014_OUTPUT.txt" },
			{ "015_INPUT.txt", "015_OUTPUT.txt" },
			{ "016_INPUT.txt", "016_OUTPUT.txt" },
			{ "017_INPUT.txt", "017_OUTPUT.txt" },
			{ "018_INPUT.txt", "018_OUTPUT.txt" },
			{ "019_INPUT.txt", "019_OUTPUT.txt" },
			{ "020_INPUT.txt", "020_OUTPUT.txt" },
			{ "021_INPUT.txt", "021_OUTPUT.txt" },
			{ "022_INPUT.txt", "022_OUTPUT.txt" },
			{ "023_INPUT.txt", "023_OUTPUT.txt" },
			{ "024_INPUT.txt", "024_OUTPUT.txt" },
			{ "025_INPUT.txt", "025_OUTPUT.txt" },
			{ "026_INPUT.txt", "026_OUTPUT.txt" },
			{ "027_INPUT.txt", "027_OUTPUT.txt" },
			{ "028_INPUT.txt", "028_OUTPUT.txt" },
			{ "029_INPUT.txt", "029_OUTPUT.txt" },
			{ "030_INPUT.txt", "030_OUTPUT.txt" },
			{ "031_INPUT.txt", "031_OUTPUT.txt" },
			{ "032_INPUT.txt", "032_OUTPUT.txt" },
			{ "033_INPUT.txt", "033_OUTPUT.txt" },
			{ "034_INPUT.txt", "034_OUTPUT.txt" },
			{ "035_INPUT.txt", "035_OUTPUT.txt" },
			{ "036_INPUT.txt", "036_OUTPUT.txt" },
			{ "037_INPUT.txt", "037_OUTPUT.txt" },
			{ "038_INPUT.txt", "038_OUTPUT.txt" },
			{ "039_INPUT.txt", "039_OUTPUT.txt" },
			{ "040_INPUT.txt", "040_OUTPUT.txt" },
			{ "041_INPUT.txt", "041_OUTPUT.txt" },
			{ "042_INPUT.txt", "042_OUTPUT.txt" },
			{ "043_INPUT.txt", "043_OUTPUT.txt" },
			{ "044_INPUT.txt", "044_OUTPUT.txt" },
			{ "045_INPUT.txt", "045_OUTPUT.txt" },
			{ "046_INPUT.txt", "046_OUTPUT.txt" },
			{ "047_INPUT.txt", "047_OUTPUT.txt" },
			{ "048_INPUT.txt", "048_OUTPUT.txt" },
			{ "049_INPUT.txt", "049_OUTPUT.txt" },
			{ "050_INPUT.txt", "050_OUTPUT.txt" },
			{ "051_INPUT.txt", "051_OUTPUT.txt" },
			{ "052_INPUT.txt", "052_OUTPUT.txt" },
			{ "053_INPUT.txt", "053_OUTPUT.txt" },
			{ "054_INPUT.txt", "054_OUTPUT.txt" },
			{ "055_INPUT.txt", "055_OUTPUT.txt" },
			{ "056_INPUT.txt", "056_OUTPUT.txt" },
			{ "057_INPUT.txt", "057_OUTPUT.txt" },
			{ "058_INPUT.txt", "058_OUTPUT.txt" },
			{ "059_INPUT.txt", "059_OUTPUT.txt" },
			{ "060_INPUT.txt", "060_OUTPUT.txt" },
			{ "061_INPUT.txt", "061_OUTPUT.txt" },
		});
	}

	@Parameter
	public String inputFile;
	@Parameter(1)
	public String expectedBoardStateFile;

	@Test
	public void validInputCase() {
		try {
			final int inputCount = countNumberOfInputRound(this.inputFile);
			try (final InputStream is = createInputStreamFromFile(this.inputFile)) {
				final GameEngine engine = new GameEngine(is, this.os);
				IntStream.range(0, inputCount).forEach((i) -> engine.nextRound());
				engine.dumpBoard(this.captureWriter);
				assertBoardState(this.expectedBoardStateFile);
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	protected String getCaseFolder() {
		return CASE_DIR;
	}

}
