package learn.reversi;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class CoordinateCodecColumnLabelTest {
	@Parameters(name = "{index}: columnIndexToLabel({0})={1}")
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] {
			{ -1, null },
			{ 0, "a" },
			{ 1, "b" },
			{ 2, "c" },
			{ 3, "d" },
			{ 4, "e" },
			{ 5, "f" },
			{ 6, "g" },
			{ 7, "h" },
		});
	}

    @Parameter
    public int colIndex;
    @Parameter(1)
    public String expectedColumnLabel;

	@Test
	public void encodeColumnLabel() {
		assertThat("Invalid column label encoded",
				CoordinateCodec.columnIndexToLabel(this.colIndex),
				equalTo(this.expectedColumnLabel));
	}

}
