package learn.reversi;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class GameEngineInvalidInputTest extends AbstractGameEngineTest {
	private static final String CASE_DIR = "src/test/resources/game002";

	@Parameters(name = "{index}: input:{0} expected output:{1}")
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] {
			{ "001_INPUT.txt", "001_OUTPUT.txt" },
			{ "002_INPUT.txt", "002_OUTPUT.txt" },
			{ "003_INPUT.txt", "003_OUTPUT.txt" },
			{ "004_INPUT.txt", "004_OUTPUT.txt" },
		});
	}

	@Parameter
	public String inputFile;
	@Parameter(1)
	public String expectedBoardStateFile;

	@Test
	public void validInputCase() {
		try {
			try (final InputStream is = createInputStreamFromFile(this.inputFile)) {
				final GameEngine engine = new GameEngine(is, this.os);
				engine.nextRound();
				engine.dumpBoard(this.captureWriter);
				assertBoardState(this.expectedBoardStateFile);
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	protected String getCaseFolder() {
		return CASE_DIR;
	}
}
