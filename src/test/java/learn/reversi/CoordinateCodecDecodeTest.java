package learn.reversi;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class CoordinateCodecDecodeTest {
	@Parameters(name = "{index}: decode({0})={1}")
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] {
			{ "1a", Coordinate.of(0, 0) },
			{ "a1", Coordinate.of(0, 0) },
			{ "8a", Coordinate.of(0, 7) },
			{ "a8", Coordinate.of(0, 7) },
			{ "1h", Coordinate.of(7, 0) },
			{ "h1", Coordinate.of(7, 0) },
			{ "8h", Coordinate.of(7, 7) },
			{ "h8", Coordinate.of(7, 7) },
			{ null, null },
			{ "", null },
			{ "111", null },
		});
	}

    @Parameter
    public String input;
    @Parameter(1)
    public Coordinate expectedCoordinate;

	@Test
	public void encodeColumnLabel() {
		assertThat("Invalid column label encoded",
				CoordinateCodec.decode(this.input),
				equalTo(this.expectedCoordinate));
	}
}
