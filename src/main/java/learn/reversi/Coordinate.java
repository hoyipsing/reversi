package learn.reversi;

public final class Coordinate {
	public final int colIndex;
	public final int rowIndex;

	private Coordinate(int colIndex, int rowIndex) {
		this.colIndex = colIndex;
		this.rowIndex = rowIndex;
	}

	public static Coordinate of(int colIndex, int rowIndex) {
		return new Coordinate(colIndex, rowIndex);
	}

	public Coordinate move(Direction direction) {
		return direction.move(this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + colIndex;
		result = prime * result + rowIndex;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Coordinate other = (Coordinate) obj;
		if (colIndex != other.colIndex) {
			return false;
		}
		if (rowIndex != other.rowIndex) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Coordinate [colIndex=" + colIndex + ", rowIndex=" + rowIndex + "]";
	}

}
