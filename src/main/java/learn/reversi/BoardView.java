package learn.reversi;

public interface BoardView {

	int getWidth();

	int getHeight();

	Cell cellAt(Coordinate coordinate);

}
