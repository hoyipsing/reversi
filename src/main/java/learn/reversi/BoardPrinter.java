package learn.reversi;

import java.io.PrintWriter;
import java.util.stream.IntStream;

public class BoardPrinter {
	private static final char SPACE = ' ';
	private static final char UNOCCUPIED = '-';

	public void print(BoardView boardView, PrintWriter printWriter) {
		for (int row = 0; row < boardView.getHeight(); ++row) {
			printWriter.print(CoordinateCodec.rowIndexToLabel(row));
			printSpace(1, printWriter);
			for (int col = 0; col < boardView.getWidth(); ++col) {
				final Cell cell = boardView.cellAt(Coordinate.of(col, row));
				print(cell, printWriter);
			}
			printWriter.println();
		}
		printSpace(2, printWriter);
		for (int col = 0; col < boardView.getWidth(); ++col) {
			printWriter.print(CoordinateCodec.columnIndexToLabel(col));
		}
		printWriter.println();
	}

	private void printSpace(int count, PrintWriter printWriter) {
		IntStream.range(0, count).forEach((i) -> printWriter.print(SPACE));
	}

	private void print(Cell cell, PrintWriter printWriter) {
		if (cell.getOccupant() == null) {
			printWriter.print(UNOCCUPIED);
		} else {
			printWriter.print(cell.getOccupant().getLabel());
		}
	}

}

