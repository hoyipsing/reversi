package learn.reversi;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.Map;
import java.util.Scanner;

public class GameEngine {
	private static final int BOARD_WIDTH = 8;
	private static final int BOARD_HEIGHT = 8;

	private final Board board = new Board(BOARD_WIDTH, BOARD_HEIGHT);
	private boolean gameOver;
	private Side currentTurn;
	private final BoardPrinter boardPrinter = new BoardPrinter();
	private final Scanner scanner;
	private final PrintWriter outputWriter;

	public GameEngine(InputStream is, OutputStream os) {
		this.scanner = new Scanner(is, StandardCharsets.UTF_8.name());
		this.outputWriter = new PrintWriter(
				new OutputStreamWriter(os, StandardCharsets.UTF_8), true);
		reset();
	}

	public void reset() {
		this.gameOver = false;
		this.currentTurn = Side.DARK;
		this.board.reset();
		this.placeDisc(CoordinateCodec.decode("4d"), Side.LIGHT);
		this.placeDisc(CoordinateCodec.decode("5e"), Side.LIGHT);
		this.placeDisc(CoordinateCodec.decode("4e"), Side.DARK);
		this.placeDisc(CoordinateCodec.decode("5d"), Side.DARK);
	}

	private void placeDisc(Coordinate coordinate, Side side) {
		this.board.placeDisc(coordinate, side);
		capture(coordinate, side);
	}

	private void capture(Coordinate coordinate, Side side) {
		Arrays.stream(Direction.values()).forEach(
				(direction) -> capture(direction.move(coordinate), side, direction, 1, false));
	}

	private boolean capture(Coordinate coordinate, Side side, Direction direction, int depth, boolean readOnly) {
		if (!this.board.isValidCoordinate(coordinate)) {
			return false;
		}
		final Cell cell = this.board.cellAt(coordinate);
		if (!cell.isOccupied()) {
			return false;
		}
		if (cell.getOccupant() == side) {
			return depth > 1;
		}
		final boolean captureSuccess = capture(direction.move(coordinate), side, direction, depth + 1, readOnly);
		if (captureSuccess && !readOnly) {
			cell.setOccupant(side);
		}
		return captureSuccess;
	}

	public boolean nextRound() {
		if (this.gameOver) {
			return false;
		}
		printBoard();

		while (true) {
			final String input = promptNextMoveFromUser();
			final Coordinate coordinate = CoordinateCodec.decode(input);
			if (isValidMove(coordinate, this.currentTurn)) {
				placeDisc(coordinate, this.currentTurn);
				upateGameState();
				break;
			} else {
				outputWriter.println(Messages.INVALID_MOVE + input);
				outputWriter.println();
			}
		}
		return !this.gameOver;
	}

	private void printBoard() {
		dumpBoard(this.outputWriter);
		this.outputWriter.println();
	}

	public void dumpBoard(PrintWriter printWriter) {
		this.boardPrinter.print(this.board, printWriter);
	}

	private void upateGameState() {
		final Side nextSide = this.currentTurn == Side.DARK ? Side.LIGHT : Side.DARK;
		if (hasEligibleMove(nextSide)) {
			this.currentTurn = nextSide;
		} else if (!hasEligibleMove(this.currentTurn)) {
			this.gameOver = true;
			printBoard();
			printScore();
		}
	}

	private void printScore() {
		this.outputWriter.println(Messages.NO_MOVE_AVAILABLE);
		final ScoreVisitor visitor = new ScoreVisitor();
		this.board.visit(visitor);
		final int lightScore = visitor.getCaptureCount(Side.LIGHT);
		final int darkScore = visitor.getCaptureCount(Side.DARK);

		if (lightScore == darkScore) {
			this.outputWriter.println(Messages.DRAW_GAME);
		} else if (lightScore < darkScore) {
			final Side winner = lightScore < darkScore ? Side.DARK : Side.LIGHT;
			final Side loser = lightScore < darkScore ? Side.LIGHT : Side.DARK;
			this.outputWriter.println(String.format(
					Messages.PLAYER_WIN, winner, visitor.getCaptureCount(winner),
					visitor.getCaptureCount(loser)));
		}
	}

	private boolean hasEligibleMove(Side side) {
		final ValidMoveVisitor visitor = new ValidMoveVisitor(side);
		this.board.visit(visitor);
		return visitor.validMoveExist;
	}

	private boolean isValidMove(Coordinate coordinate, Side side) {
		final Cell cell = this.board.cellAt(coordinate);
		if (cell.isOccupied()) {
			return false;
		}
		for (Direction direction : Direction.values()) {
			if (capture(direction.move(coordinate), side, direction, 1, true)) {
				return true;
			}
		}
		return false;
	}

	private String promptNextMoveFromUser() {
		this.outputWriter.print(String.format(Messages.INPUT_NEXT_MOVE, this.currentTurn.getLabel()));
		this.outputWriter.flush();
		return this.scanner.nextLine();
	}

	class ValidMoveVisitor implements BoardVisitor {
		private final Side side;
		private boolean validMoveExist;

		ValidMoveVisitor(Side side) {
			this.side = side;
		}

		@Override
		public boolean onCell(Cell cell) {
			if (isValidMove(cell.getCoordinate(), this.side)) {
				this.validMoveExist = true;
				return false;
			}
			return true;
		}
	}

	static class ScoreVisitor implements BoardVisitor {
		private final Map<Side, Integer> captures = new EnumMap<>(Side.class);

		@Override
		public boolean onCell(Cell cell) {
			if (cell.isOccupied()) {
				Integer count = this.captures.get(cell.getOccupant());
				if (count == null) {
					count = 0;
				}
				this.captures.put(cell.getOccupant(), count + 1);
			}
			return true;
		}

		private int getCaptureCount(Side side) {
			Integer count = this.captures.get(side);
			return count == null ? 0 : count.intValue();
		}

	}

}
