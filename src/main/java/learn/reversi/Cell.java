package learn.reversi;

import java.util.EnumMap;
import java.util.Map;

public class Cell {
	private final Coordinate coordinate;
	private final Map<Direction, Cell> neighbours = new EnumMap<>(Direction.class);
	private Side occupant;

	public Cell(Coordinate coordinate) {
		this.coordinate = coordinate;
	}

	public Side getOccupant() {
		return occupant;
	}

	public void setOccupant(Side occupant) {
		this.occupant = occupant;
	}

	public boolean isOccupied() {
		return this.occupant != null;
	}

	public void clearOccupancy() {
		this.occupant = null;
	}

	public Coordinate getCoordinate() {
		return coordinate;
	}

	public void setNeighbour(Direction direction, Cell cell) {
		this.neighbours.put(direction, cell);
	}

}
