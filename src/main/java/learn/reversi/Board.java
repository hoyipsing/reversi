package learn.reversi;

import java.util.HashMap;
import java.util.Map;

public class Board implements BoardView {
	private final int width;
	private final int height;
	private final Map<Coordinate, Cell> cells;

	public Board(int width, int height) {
		this.width = width;
		this.height = height;
		this.cells = new HashMap<>(width * height);
		createEmptyBoard();
		linkCell();
	}

	private void createEmptyBoard() {
		for (int col = 0; col < width; ++col) {
			for (int row = 0; row < height; ++row) {
				final Coordinate coordinate = Coordinate.of(col, row);
				this.cells.put(coordinate, new Cell(coordinate));
			}
		}
	}

	private void linkCell() {
		for (Cell cell : this.cells.values()) {
			final Coordinate coordinate = cell.getCoordinate();
			for (Direction direction : Direction.values()) {
				final Coordinate neighbourCoordinate = coordinate.move(direction);
				if (isValidCoordinate(neighbourCoordinate)) {
					cell.setNeighbour(direction, cell);
				}
			}
		}
	}

	public void visit(BoardVisitor visitor) {
		for (Cell cell : this.cells.values()) {
			if (!visitor.onCell(cell)) {
				break;
			}
		}
	}

	public boolean isValidCoordinate(Coordinate coordinate) {
		return coordinate.colIndex >= 0
				&& coordinate.colIndex < this.width
				&& coordinate.rowIndex >= 0
				&& coordinate.rowIndex < this.height;
	}

	@Override
	public int getWidth() {
		return this.width;
	}

	@Override
	public int getHeight() {
		return this.height;
	}

	@Override
	public Cell cellAt(Coordinate coordinate) {
		return this.cells.get(coordinate);
	}

	public void placeDisc(Coordinate coordinate, Side side) {
		this.cells.get(coordinate).setOccupant(side);
	}

	public void reset() {
		clearOccupancy();
	}

	private void clearOccupancy() {
		this.cells.values().forEach(Cell::clearOccupancy);
	}

}
