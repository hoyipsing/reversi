package learn.reversi;

public interface BoardVisitor {

	boolean onCell(Cell cell);

}
