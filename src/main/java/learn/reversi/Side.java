package learn.reversi;

public enum Side {
	LIGHT("O"), DARK("X");

	private final String label;

	Side(String label) {
		this.label = label;
	}

	public String getLabel() {
		return this.label;
	}
}
