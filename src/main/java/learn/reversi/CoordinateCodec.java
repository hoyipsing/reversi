package learn.reversi;

public final class CoordinateCodec {

	private CoordinateCodec() {
	}

	public static Coordinate decode(String input) {
		if (input == null || input.length() != 2) {
			return null;
		}
		if (Character.isDigit(input.charAt(0))) {
			return Coordinate.of(
					columnLabelToIndex(input.charAt(1)),
					rowLabelToIndex(input.charAt(0)));
		} else {
			return Coordinate.of(
					columnLabelToIndex(input.charAt(0)),
					rowLabelToIndex(input.charAt(1)));
		}
	}

	public static String encode(Coordinate coordinate) {
		return null;
	}

	private static int columnLabelToIndex(char columnLabel) {
		return columnLabel - 'a';
	}

	private static int rowLabelToIndex(char rowLabel) {
		return rowLabel - '1';
	}

	public static String columnIndexToLabel(int colIndex) {
		if (colIndex < 0) {
			return null;
		}
		return Character.toString((char) (colIndex + 'a'));
	}

	public static String rowIndexToLabel(int rowIndex) {
		if (rowIndex < 0) {
			return null;
		}
		return String.valueOf(rowIndex + 1);
	}

}
