package learn.reversi;

public enum Direction {
	TOP {
		@Override
		Coordinate move(Coordinate coordinate) {
			return Coordinate.of(coordinate.colIndex, coordinate.rowIndex - 1);
		}
	},
	LEFT {
		@Override
		Coordinate move(Coordinate coordinate) {
			return Coordinate.of(coordinate.colIndex - 1, coordinate.rowIndex);
		}
	},
	BOTTOM {
		@Override
		Coordinate move(Coordinate coordinate) {
			return Coordinate.of(coordinate.colIndex, coordinate.rowIndex + 1);
		}
	},
	RIGHT {
		@Override
		Coordinate move(Coordinate coordinate) {
			return Coordinate.of(coordinate.colIndex + 1, coordinate.rowIndex);
		}
	},
	TOP_RIGHT {
		@Override
		Coordinate move(Coordinate coordinate) {
			return Coordinate.of(coordinate.colIndex + 1, coordinate.rowIndex - 1);
		}
	},
	BOTTOM_RIGHT {
		@Override
		Coordinate move(Coordinate coordinate) {
			return Coordinate.of(coordinate.colIndex + 1, coordinate.rowIndex + 1);
		}
	},
	BOTTOM_LEFT {
		@Override
		Coordinate move(Coordinate coordinate) {
			return Coordinate.of(coordinate.colIndex - 1, coordinate.rowIndex + 1);
		}
	},
	TOP_LEFT {
		@Override
		Coordinate move(Coordinate coordinate) {
			return Coordinate.of(coordinate.colIndex - 1, coordinate.rowIndex - 1);
		}
	};

	abstract Coordinate move(Coordinate coordinate);
}
