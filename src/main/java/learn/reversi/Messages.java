package learn.reversi;

public final class Messages {

	private Messages() {
	}

	public static final String INVALID_MOVE = "Invalid move. Please try again.";
	public static final String INPUT_NEXT_MOVE = "Player '%s' move: ";
	public static final String NO_MOVE_AVAILABLE = "No further moves available";
	public static final String PLAYER_WIN = "Player '%s' wins ( %d vs %d )";
	public static final String DRAW_GAME = "Draw game";
}
